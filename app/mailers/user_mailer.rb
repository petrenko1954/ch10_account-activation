#Листинг 10.9: Созданный User mailer. app/mailers/user_mailer.rbЛистинг 10.11: Отправка ссылки на активацию аккаунта. app/mailers/user_mailer.rbЛистинг 10.11: Отправка ссылки на активацию аккаунта. app/mailers/user_mailer.rb 
  
class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  
#Jan 10 
 def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset
    @greeting = "Hi"

    mail to: "to@example.org"
  end
end
